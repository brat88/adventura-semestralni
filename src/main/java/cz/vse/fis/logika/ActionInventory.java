package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro zobrazení věcí v inventáři.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-25-10
 */


public class ActionInventory implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionInventory(Game game)
    {
        this.game = game;
    }
    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>inventar</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "inventar";
    }

    /**
     * Metoda vrací váš inventář.
     *
     * @param parameters parametry příkazu <i>(aktuálně se ignorují)</i>
     * @return nápověda, kterou hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if(parameters.length == 0) {
            if (game.getWorld().getInventory().getItems().isEmpty()) {
                return "Inventář je prázdný. \nInventář má velikost: " + game.getWorld().getInventory().getMaxInvSize();
            } else {
                return "Inventář obsahuje " + game.getWorld().getInventory().getItems() + ". \nInventář má velikost: " + game.getWorld().getInventory().getMaxInvSize();
            }
        }
        return "Tento příkaz nesmí mít parametr";
    }
}
