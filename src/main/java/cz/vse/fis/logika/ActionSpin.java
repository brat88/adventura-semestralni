package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro roztočení herního automatu.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-26
 */

public class ActionSpin implements IAction{
    private Game game;
    private Inventory inventory;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionSpin(Game game)
    {
        this.game = game;
        this.inventory = game.getWorld().getInventory();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>roztocit</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "roztocit";
    }

    /**
     * Metoda roztočí automat, pomocí předmětu v inventáři. Z automatu vypadne předmět, který se dá do inventáře.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s dvěma prvky)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */

    @Override
    public String execute(String[] parameters)
    {
        Area currentArea = game.getWorld().getCurrentArea();
        if(parameters.length == 0)
        {
            return "Musíš zadat, jaký automat, chceš roztočit.";
        }
        else
        {
            String automat = parameters[0];
            if(parameters.length == 1)
            {
                return "Automat se neroztočí jen tak. Zjisti čím ho musíš roztočit.";
            }
            else
            {
                if(parameters.length > 2)
                {
                    return "Tento příkaz musí mít dva parametry.";
                }

                String coin = parameters[1];

                if(!currentArea.containsItem(automat))
                {
                    return "V této místnosti se nenachází předmět '" + automat + "'.";
                }
                else{
                    if(!currentArea.getItem(automat).isNoAutomat())
                    {
                        if(inventory.containItem(coin))
                        {
                            if(currentArea.getItem(automat).getPrice().equals(coin))
                            {
                                inventory.delItem(coin);
                                Item win = currentArea.getItem(automat).getItemUvnitr();
                                currentArea.getItem(automat).removeItemUvnitr();
                                inventory.addToInv(win);

                                return "Vyhrál si tento předmět '" + win.getName() + "'.";
                            }
                            else{
                                return "Tímto předmětem nelze roztočit automat.";
                            }
                        }
                        else{
                            return "Předmět, kterým chceš roztočit automat nemáš v inventáři.";
                        }
                    }
                    else{
                        return "Předmět není automat.";
                    }
                }



            }

        }

    }
}

