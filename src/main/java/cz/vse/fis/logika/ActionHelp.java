package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro zobrazení nápovědy ke hře.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-25-10
 */
public class ActionHelp implements IAction
{
    private Game game;
    private static final String LOS = "Vyherni_los";

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionHelp(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>napoveda</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "napoveda";
    }

    /**
     * Metoda vrací obecnou nápovědu ke hře. Nyní vypisuje příkazy, které lze použít
     * a primitivní zprávu o herním příběhu.
     *
     * @param parameters parametry příkazu
     * @return nápověda, kterou hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length < 1) {
            if (game.getWorld().getInventory().containItem(LOS)) {
                return "Zkus najít něco čím bys mohl zapít svoji výhru\n" +
                        "Tvým úkolem je zbohatnout, to jak to uděláš je jenom na tobě,"
                        + "\nale pozor ať u toho nepřijdeš na mizinu." +
                        "\nPokud vyhraješ nějakou výhru, tak by to chtělo i nějak oslavit."
                        + "\n\nPříkazy jaké můžeš použít jsou: seber, poloz, inventar, prozkoumej, rozhledni_se, jdi, konec, koupit, roztoc, setri";

            }
            else{
                return "Tvým úkolem je zbohatnout, to jak to uděláš je jenom na tobě,"
                        + "\nale pozor ať u toho nepřijdeš na mizinu." +
                        "\nPokud vyhraješ nějakou výhru, tak by to chtělo i nějak oslavit."
                        + "\n\nPříkazy jaké můžeš použít jsou: seber, poloz, inventar, prozkoumej, rozhledni_se, jdi, konec, koupit, roztoc, setri";

            }
        }
        if (parameters.length > 1)
        {
            return "Tomu nerozumím, musíš zadat pouze jeden parametr.";
        }
        String nazevPrikazu = parameters[0];
        String odpoved = "Zadaný paremetr neexistuje.";

        switch (nazevPrikazu)
        {
            case "seber":
                odpoved = "Tento příkaz lze použít pro sebrání věcí, co najdeš.";
                break;
            case "poloz":
                odpoved = "Tento příkaz slouží k odložení věcí z inventáře.";
                break;
            case "inventar":
                odpoved = "Tímto příkazem zjistíš, co máš v inventáři.";
                break;
            case "prozkoumej":
                odpoved = "Tento příkaz prozkoumá věc, kterou zvolíš.";
                break;
            case "rozhledni_se":
                odpoved = "Tímto příkazem se rozhlednéš po okolí.";
                break;
            case "jdi":
                odpoved = "Tímto příkazem jdeš do jíné lokace.";
                break;
            case "konec":
                odpoved = "Tento příkaz ukončí hru.";
                break;
            case "koupit":
                odpoved = "Tento příkaz koupí předmět.";
                break;
            case "roztoc":
                odpoved = "Tento příkaz roztočí automat.";
                break;
            case "setri":
                odpoved = "Tento příkaz setře stírací los.";
                break;
        }
        return odpoved;
    }
}
