package cz.vse.fis.logika;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * Během hry třída vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-27
 */
public class Game
{
    private boolean gameOver;
    private GameWorld world;

    private Set<IAction> actions;

    /**
     * Konstruktor třídy, vytvoří hru a množinu platných příkazů. Hra po
     * vytvoření již běží a je připravená zpracovávat herní příkazy.
     */
    public Game()
    {
        gameOver = false;
        world = new GameWorld();

        actions = new HashSet<>();
        actions.add(new ActionHelp(this));
        actions.add(new ActionTerminate(this));
        actions.add(new ActionMove(this));
        actions.add(new ActionInvestigate(this));
        actions.add(new ActionLookAround(this));
        actions.add(new ActionPick(this));
        actions.add(new ActionInventory(this));
        actions.add(new ActionDrop(this));
        actions.add(new ActionBuy(this));
        actions.add(new ActionSpin(this));
        actions.add(new ActionScratch(this));
    }

    /**
     * Metoda vrací informaci, zda hra již skončila <i>(je jedno, jestli
     * výhrou, prohrou nebo příkazem 'konec')</i>.
     *
     * @return {@code true}, pokud hra již skončila; jinak {@code false}
     */
    public boolean isGameOver()
    {
        return gameOver;
    }


    public void setGameOver(boolean gameOver)
    {
        this.gameOver = gameOver;
    }

    /**
     * Metoda vrací odkaz na mapu prostorů herního světa.
     *
     * @return mapa prostorů herního světa
     */
    public GameWorld getWorld()
    {
        return world;
    }

    /**
     * Metoda zpracuje herní příkaz <i>(jeden řádek textu zadaný na konzoli)</i>.
     * Řetězec uvedený jako parametr se rozdělí na slova. První slovo je považováno
     * za název příkazu, další slova za jeho parametry.
     * <p>
     * Metoda nejprve ověří, zda první slovo odpovídá hernímu příkazu <i>(např.
     * 'napoveda', 'konec', 'jdi' apod.)</i>. Pokud ano, spustí obsluhu tohoto
     * příkazu a zbývající slova předá jako parametry.
     *
     * @param line text, který hráč zadal na konzoli jako příkaz pro hru
     * @return výsledek zpracování <i>(informace pro hráče, které se vypíšou na konzoli)</i>
     */
    public String processAction(String line)
    {
        String[] words = line.split("[ \t]+");

        String actionName = words[0];
        String[] actionParameters = new String[words.length - 1];

        for (int i = 0; i < actionParameters.length; i++) {
            actionParameters[i] = words[i + 1];
        }

        String result = "Tomu nerozumím, tento příkaz neznám.";

        for (IAction executor : actions) {
            if (executor.getName().equals(actionName)) {
                result = executor.execute(actionParameters);
            }
        }

        if (world.getGameState() != GameState.PLAYING) {
            gameOver = true;
        }

        return result;
    }

    /**
     * Metoda vrací úvodní text pro hráče, který se vypíše na konzoli ihned po
     * zahájení hry.
     *
     * @return úvodní text
     */
    public String getPrologue()
    {
        return "Vítejte!\n"
                + "Toto je příběh o skladníkovi, který se chce stát milionářem.\n"
                + "Nevíte-li, jak pokračovat, zadejte příkaz 'nápověda'.";
    }

    /**
     * Metoda vrací závěrečný text pro hráče, který se vypíše na konzoli po ukončení
     * hry. Metoda se zavolá pro všechna možná ukončení hry <i>(hráč vyhrál, hráč
     * prohrál, hráč ukončil hru příkazem 'konec')</i>. Tyto stavy je vhodné
     * v metodě rozlišit.
     *
     * @return závěrečný text
     */
    public String getEpilogue()
    {
        String epilogue = "Díky, že sis zahrál.";

        GameState gameState = world.getGameState();

        if (gameState == GameState.WON) {
            epilogue = "Vyhrál jsi. \n Vyherní los ti zajistil, že po dobu dalších 20 let budeš dostávat 100 000 Kč měsíčně a zároveň i okamžitou výhru 5 000 000 Kč.\n" +
                    "Teď už si můžeš jenom užívat a začal bych tím drinkem, co jsis vzal." +
                    "\n" + epilogue;
        }

        if (gameState == GameState.LOST) {
            epilogue = "Prohrál jsi a jseš na mizině. Měl bys začít víc pracovat, protože ty štestí rozhdoně nemáš.\n\n" + epilogue;
        }

        if(gameState == GameState.SMALL_WIN)
        {
            epilogue = "Není to žádná sláva, ale aspoň máš tenhle měsíc na nájem. \n\n" + epilogue;
        }

        return epilogue;
    }
}
