package cz.vse.fis.logika;

public class Item implements Comparable<Item>
{
    private String name;
    private String description;
    private boolean moveable;
    private String price;
    private Item itemUvnitr;
    private Item itemUvnitr2;
    private boolean noAutomat;
    private boolean noLos;
    private String mince;
    private String mince2;
    private String obrazek;


    public Item(String name, String description, boolean moveable, String price, Item itemUvnitr, Item itemUvnitr2, boolean noAutomat, boolean noLos, String mince, String mince2, String obrazek)
    {
        this.name = name;
        this.description = description;
        this.moveable = moveable;
        this.price = price;
        this.itemUvnitr = itemUvnitr;
        this.itemUvnitr2 = itemUvnitr2;
        this.noAutomat = noAutomat;
        this.noLos = noLos;
        this.mince = mince;
        this.mince2 = mince2;
        this.obrazek = obrazek;
    }

    public Item(String name, String description, Item itemUvnitr, Item itemUvnitr2, String obrazek)
    {
        this(name, description, true, "", itemUvnitr, itemUvnitr2, true, true, "", "", obrazek);
    }

    public String getName()
    {
        return name;
    }

    public Item getItemUvnitr() { return itemUvnitr;}

    public Item getItemUvnitr2() { return itemUvnitr2; }

    public String getDescription()
    {
        return description;
    }

    public String getPrice(){ return price;}

    public String getMince(){return mince;}

    public String getMince2(){ return mince2;}

    public boolean isNoAutomat(){ return noAutomat;}

    public boolean isNoLos(){return noLos;}

    public void setPrice(String price) { this.price = price; }

    public boolean isMoveable()
    {
        return moveable;
    }

    public void removeItemUvnitr() { itemUvnitr = null;}

    public String getObrazek()
    {
        return obrazek;
    }
    @Override
    public boolean equals(final Object o)
    {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (o instanceof Item) {
            Item item = (Item) o;

            return name.equals(item.getName());
        }

        return false;
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }

    @Override
    public int compareTo(Item item)
    {
        return name.compareTo(item.getName());
    }
}
