package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro kupování předmětů.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-26
 */

public class ActionBuy implements IAction{
    private Game game;
    private Inventory inventory;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionBuy(Game game)
    {
        this.game = game;
        this.inventory = game.getWorld().getInventory();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>koupit</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "koupit";
    }

    /**
     * Metoda koupí předmět, za jiný předmět, co má hráč v inventáři.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s dvěma prvky)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */

    @Override
    public String execute(String[] parameters)
    {
        Area currentArea = game.getWorld().getCurrentArea();
        if(parameters.length == 0)
        {
            return "Musíš zadat, co chceš koupit.";
        }
        else
        {
            String parametr1 = parameters[0];

            if(currentArea.containsItem(parametr1))
            {
                if(currentArea.getItem(parametr1).getPrice().equals(""))
                {
                    return "Předmět nic nestojí, použij příkaz seber";
                }
                else
                {
                    if(parameters.length == 1)
                    {
                        return "Musíš zadat předmět, kterým chceš věc koupit.";
                    }
                    else {
                        if(parameters.length > 2)
                        {
                            return "Tento příkaz musí mít dva parametry.";
                        }

                        String parametr2 = parameters[1];

                        if(inventory.containItem(parametr2))
                        {
                            if(currentArea.getItem(parametr1).getPrice().equals(parametr2))
                            {
                                Item item1 = currentArea.getItem(parametr1);
                                Item item2 = inventory.getItem(parametr2);

                                currentArea.removeItem(parametr1);
                                inventory.delItem(parametr2);

                                currentArea.addItem(item2);
                                inventory.addToInv(item1);

                                item1.setPrice("");
                                item2.setPrice(parametr1);

                                return "Koupil jsi předmět '" + parametr1 + "' za '" + parametr2 + "'.";
                            }
                        }
                        else{
                            return "Předmět, kterým si chceš koupit jiný předmět, nemáš v inventáři.";
                        }
                    }
                }

            }
            else
            {
                return "Předmět není v této místnosti.";
            }
        }
        return "Obchod se nezdařil, jelikož nemáš věc, za kterou můžeš koupit předmět.";
    }
}
