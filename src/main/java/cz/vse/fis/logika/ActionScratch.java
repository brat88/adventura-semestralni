package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro setření losu.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-27
 */

public class ActionScratch implements IAction{
    //private Game game;
    private Inventory inventory;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionScratch(Game game)
    {
        //this.game = game;
        this.inventory = game.getWorld().getInventory();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>setrit</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "setrit";
    }

    /**
     * Metoda setře los, pomocí předmětu v inventáři. Los vymaže z inventáře a ukáže se vám výhra nebo prohra.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s dvěma prvky)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */

    @Override
    public String execute(String[] parameters)
    {
        if(parameters.length == 0)
        {
            return "Musíš zadat, jaký los chceš setřít.";
        }
        else
        {
            String los = parameters[0];
            if(parameters.length == 1)
            {
                return "Los se nesetře jen tak. Zjisti čím ho musíš setřít.";
            }
            else
            {
                if(parameters.length > 2)
                {
                    return "Tento příkaz musí mít dva parametry.";
                }

                String coin = parameters[1];

                if(inventory.containItem(los) && inventory.containItem(coin))
                {
                    Item itemLos = inventory.getItem(los);

                    if(itemLos.isNoLos())
                    {
                        return "Předmět '" + los + "' není stírací los.";
                    }
                    else{
                        if(itemLos.getMince().equals(coin))
                        {
                            Item win = itemLos.getItemUvnitr();

                            inventory.delItem(los);
                            inventory.addToInv(win);

                            return "Setřel si los a " + win.getDescription();
                        }
                        else
                        {
                            if(itemLos.getMince2().equals(coin))
                            {
                                Item win = itemLos.getItemUvnitr2();
                                inventory.delItem(los);
                                inventory.addToInv(win);
                                return "Setřel si los a " + win.getDescription();
                            }

                            else{
                                return "Tímto předmětem nemůžeš setřít los.";

                        }

                        }
                    }
                }
                else{
                    if(inventory.containItem(los))
                    {
                        return "V inventáři nemáš předmět '" + coin + "'.";
                    }
                    else{
                        if(inventory.containItem(coin))
                        {
                            return "V inventáři nemáš předmět '" + los + "'.";
                        }
                        else {
                            return "V inventáři nemáš ani jeden předmět.";
                        }
                    }
                }
            }

        }

    }
}
