package cz.vse.fis.logika;

import cz.vse.fis.main.Pozorovatel;
import cz.vse.fis.main.PredmetPozorovani;
import java.util.Set;
import java.util.HashSet;




/**
 * Třída představující mapu lokací herního světa. V datovém atributu
 * {@link #currentArea} uchovává odkaz na aktuální lokaci, ve které
 * se hráč právě nachází. Z aktuální lokace je možné se prostřednictvím
 * jejích sousedů dostat ke všem přístupným lokacím ve hře.
 * <p>
 * Veškeré informace o stavu hry <i>(mapa prostorů, inventář, vlastnosti
 * hlavní postavy, informace o plnění úkolů apod.)</i> by měly být uložené
 * zde v podobě datových atributů.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-27
 */
public class GameWorld implements PredmetPozorovani
{
    private Area currentArea;
    private Inventory currentInv;
    private Set<Pozorovatel> seznamPozorovatelu = new HashSet<>();

    /**
     * Konstruktor třídy, vytvoří jednotlivé lokace a propojí je pomocí východů.
     */
    public GameWorld()
    {
        currentInv = new Inventory();

        Area town = new Area("mesto", "Město, v kterém bydlíš a pracuješ.");
        Area warehouse = new Area("sklad", "V tomto skladu pracuješ.");
        Area home = new Area("domov", "Tady bydlíš.");
        Area kiosk = new Area("kiosek", "Stánek, kde si můžeš koupit různé věci.");

        Area village = new Area("vesnice", "Toto je vesnice, do které jezdíš za zábavou.");
        Area casino = new Area("herna", "Místní herna, kam chodíš trávit svůj volný čas.");
        Area slot = new Area ("forbes", "Místnost s automatem, kam chodíš velmi často točit.");
        Area roulette = new Area("ruleta", "Místnost s kuličkou, kam chodíš jenom občas.");

        town.addExit(warehouse);
        town.addExit(home);
        town.addExit(kiosk);
        town.addExit(village);

        warehouse.addExit(town);
        warehouse.addExit(kiosk);

        home.addExit(town);

        kiosk.addExit(town);
        kiosk.addExit(warehouse);

        village.addExit(casino);
        village.addExit(town);

        casino.addExit(slot);
        casino.addExit(roulette);
        casino.addExit(village);

        slot.addExit(casino);
        slot.addExit(roulette);

        roulette.addExit(casino);
        roulette.addExit(slot);

        currentArea = warehouse;

        // Věci sklad
        Item bryle = new Item("bryle", "Tvoje brýle, které nosíš protože nevidíš.",null, null, "bryle.png");
        Item vyplata = new Item("tisicovka", "Tohle je tvoje výplata, tak si ji vezmi.",null, null,"tisicovka.png");
        Item palety = new Item("palety", "Palety, které jsou fakt těžké.", false, "", null, null, true, true, "", "","");


        // Věci doma
        Item coin = new Item("zeton", "Žeton, který sis koupil naposledy v herně, ale nestihl si ho prohrát.\nNa další už bohužel nemáš prachy, tak si dobře promysli, kde ho použiješ..", null, null, "zeton.png");
        Item roll = new Item("rohlik", "Zbytek rohlíku od snídaně.", null, null,"rohlik.png");
        Item iron = new Item("zehlicka", "Stará žehlička po rodičích.", null, null,"iron.png");
        Item bed = new Item("postel", "Na týhle rozvikláný posteli spíš.", false, "", null, null, true, true, "", "","");
        Item clock = new Item("hodiny", "Nefunkční hodiny", null, null,"");

        // Věci stánek
        Item losw = new Item("Vyherni_los", "vyhrál si hroznou haldu peněz.", null, null,"");
        Item losl = new Item("Proherni_los", "vyhrál si 20 Kč.",null, null,"");
        Item malaVyhra = new Item("mala_vyhra", "vyhrál si 20 000 Kč", null, null,"");
        Item teleskop = new Item("teleskop", "Kalený teleskopický obušek", true, "tisicovka",null, null, true,true, "", "", "teleskop.png");
        Item radio = new Item("autoradio", "Tohle rádio už má něco za sebou.", true, "tisicovka", null, null, true,true, "", "", "radio.png");
        Item los1 = new Item("maxi_cerna_perla", "Každá černá perla je jedinečná, ale Maxi Černá perla může být i dvacetimilionová. Přesně taková je totiž její hlavní výhra.", true,"tisicovka",losl, losl, true,false, "kouzelna_mince", "mince", "perla.png");
        Item los2 = new Item("mega_rentier", "Los Mega Rentiér nabízí mimořádnou hlavní výhru 5 000 000 Kč hned a k tomu navíc stotisícovou měsíční rentu vyplácenou po dobu 20 let.",true,"tisicovka",losw, losl, true,false,"kouzelna_mince", "mince", "rentier.png");
        Item los3 = new Item("velky_hrac","Risk, odvaha, adrenalin, úspěch. Vždy o krok napřed, vždy vítěz situace. Právě pro vás je tu nový los HRÁČ. VELKÝ HRÁČ.\nUnikátní los našlapaný výhrami. Hlavní výhra vás nenechá chladným, je to rovných 20 000 000 Kč",true,"tisicovka",malaVyhra, losl, true,false, "kouzelna_mince", "mince", "velkyhrac.png");

        // Věci vesnice
        Item lampa = new Item("poulicni_lampa", "Obyčejná lampa", false, "", null, null, true, true, "", "","");
        Item bench = new Item("lavicka", "Špinavá lavička", false, "", null, null, true, true, "", "","");

        // Věci herna
        Item tabule = new Item("svitici_tabule", "Tabule s nápisem.",false,"",null, null, true,true, "", "","");
        Item drink = new Item("welcome_drink", "Drink na uvítanou nebo s ním případně můžeš zapít výhru",null, null, "drink.png");

        // Věci forbes a ruleta
        Item vyhra = new Item("kouzelna_mince", "Divně vypadající mince, třeba ji na něco budeš moc použít.",null, null, "kouzmince.png");
        Item prohra = new Item("mince", "",null, null, "mince.png");
        Item forbes1 = new Item("Lucky777", "Zkusíš svoje štěstí na tomhle automatu.",false,"zeton", prohra, null, false,true, "", "","");
        Item forbes2 = new Item("tresne_sypou", "Legendární automat, kde seděli legendy této herny.",false,"zeton", prohra, null, false,true, "", "","");
        Item ruleta1 = new Item("velka_ruleta", "Klasická ruleta.",false,"zeton", vyhra, null, false,true, "", "","");
        Item ruleta2 = new Item("mala_ruleta", "Klasická ruleta.",false,"zeton", prohra, null, false,true, "", "","");

        //Itemy v lokacích

        warehouse.addItem(bryle);
        warehouse.addItem(vyplata);
        warehouse.addItem(palety);

        home.addItem(coin);
        home.addItem(roll);
        home.addItem(iron);
        home.addItem(bed);
        home.addItem(clock);

        kiosk.addItem(teleskop);
        kiosk.addItem(los2);
        kiosk.addItem(los3);
        kiosk.addItem(radio);
        kiosk.addItem(los1);

        village.addItem(lampa);
        village.addItem(bench);

        casino.addItem(tabule);
        casino.addItem(drink);

        slot.addItem(forbes1);
        slot.addItem(forbes2);

        roulette.addItem(ruleta1);
        roulette.addItem(ruleta2);

/*
        Area house = new Area("dům", "Tady bydlíš, proto tu i začíná hra.");
        Area forest = new Area("les", "Toto je les kolem tvého domu, rostou zde houby, žije neškodná havěť apod.");
        Area darkForest = new Area("tmavý_les", "Toto je tmavý les, opravdu se tady necítíš dobře.");
        Area cave = new Area("jeskyně", "Toto je tmavá jeskyně, ve které žije zlý vlk.");
        Area finish = new Area("chaloupka", "Toto je chaloupka, ve které bydlí babička.");

        house.addExit(forest);

        forest.addExit(house);
        forest.addExit(darkForest);
        forest.addExit(finish);

        darkForest.addExit(forest);
        darkForest.addExit(cave);

        finish.addExit(forest);

        currentArea = house;

        Item raspberries = new Item("maliny", "Jsou to prostě obyčejné lesní maliny. Co Tě na nich tak fascinuje?");
        Item tree = new Item("strom", "Tohle je ten největší strom, který jsi v životě viděl(a).", false);

        forest.addItem(raspberries);
        forest.addItem(tree);

 */
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea()
    {
        return currentArea;
    }

    /**
     * Metoda nastaví aktuální lokaci. Používá ji příkaz {@link ActionMove}
     * při přechodu mezi lokacemi.
     *
     * @param currentArea lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area currentArea)
    {
        this.currentArea = currentArea;
        upozorniPozorovatele();
    }

    /**
     * Metoda vrací aktuální stav hry <i>(běžící hra, výhra, prohra)</i>.
     *
     * @return aktuální stav hry
     */
    public GameState getGameState()
    {
        if (currentInv.containItem("Vyherni_los") && currentInv.containItem("welcome_drink")) {
            return GameState.WON;
        }

        if (currentInv.containItem("Proherni_los")) {
            return GameState.LOST;
        }

        if(currentInv.containItem("mala_vyhra"))
        {
            return GameState.SMALL_WIN;
        }

        return GameState.PLAYING;
    }

    public Inventory getInventory()
    {
        return currentInv;
    }

    @Override
    public void registruj(Pozorovatel pozorovatel) {
        seznamPozorovatelu.add(pozorovatel);
    }

    private void upozorniPozorovatele() {
        for(Pozorovatel pozorovatel : seznamPozorovatelu) {
            pozorovatel.aktualizuj();
        }
    }

}
