package cz.vse.fis.logika;

import cz.vse.fis.main.Pozorovatel;
import cz.vse.fis.main.PredmetPozorovani;

import java.util.*;

/*******************************************************************************
 * Instance třídy Inventory představuje kolekci věcí, které si hráč přidal
 * do inventáře v průběhu hraní.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-25
 */

public class Inventory implements PredmetPozorovani{

    // Datové atributy
    private static final int VELIKOST = 2; // Maximální počet předmětů v inventáři
    private List<Item> obsah;// Seznam věcí v inventáři
    private Set<Pozorovatel> seznamPozorovatelu = new HashSet<>();


    /***************************************************************************
     *  Konstruktor
     */
    public Inventory()
    {
        obsah = new ArrayList<>();
    }

    /**
     * Metoda pro přídání věci do inventáře.
     *
     * @param   predmet    přidávaná věc
     * @return  predmet    vrátí tu samou věc, pokud se ji podaří přidat do batohu.
     *                 Null, pokud se nepodaří.
     */
    public Item addToInv(Item predmet)
        {
            if(misto())
            {
                obsah.add(predmet);
                upozorniPozorovatele();
                return predmet;
            }
            return null;
        }

    /**
     * Zjišťuje, zda je v inventáři ještě místo.
     *
     * @return  true   pokud je v inventáři místo, jinak false
     *
     */
    public boolean misto()
    {
        return obsah.size() < VELIKOST;
    }

    /**
     * Zjišťuje, zda je věc v inventáři.
     *
     * @param   predmet    věc, kterou hledáme
     * @return  true       pokud se věc v inventáři nachází, jinak false
     */
    public boolean containItem(String predmet) {

        for (Item s : obsah) {
            if (s.getName().equals(predmet)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Vrací seznam věcí v inventáři
     *
     * @return   seznam     výpis věcí, které jsou v inventáři
     */
    public String getItems() {
        String seznam = "";
        for (Item s : obsah) {
            if (!seznam.equals("")) {
                seznam += ",";
            }
            seznam += " " + s.getName();
        }
        return seznam;
    }

    /**
     * Metoda najde věc, na kterou chceme odkázat
     *
     * @param name      název věci, kterou chceme najít
     * @return predmet   odkaz na nalezenou věc. Null, pokud věc nebyla nalezena v inventáři
     */
    public Item getItem(String name) {
        Item predmet = null;
        for (Item s : obsah) {
            if(s.getName().equals(name)) {
                predmet = s;
                break;
            }
        }
        return predmet;
    }

    /**
     * Odstrani veci z inventare
     *
     * @param   name      odstraňovaná věc
     * @return  predmet     odstraněná věc. Je null, pokud věc nebyla v inventáři
     */
    public Item delItem (String name) {
        Item predmet = null;
        for (Item s : obsah) {
            if (s.getName().equals(name)) {
                predmet = s;
                obsah.remove(s);
                break;
            }
        }
        upozorniPozorovatele();
        return predmet;
    }

    /**
     * Vrací všechny předměty v inventáři
     *
     *
     * @return  obsah     inventar
     */

    public Collection<Item> getInventar()
    {
        return new HashSet<>(obsah);
    }

    /**
     * Metoda vrací maximální počet věcí, které lze přidat do inventáře.
     *
     * @return  velikost
     */
    public int getMaxInvSize() {
        return VELIKOST;
    }

    @Override
    public void registruj(Pozorovatel pozorovatel) {
        seznamPozorovatelu.add(pozorovatel);
    }

    private void upozorniPozorovatele() {
        for(Pozorovatel pozorovatel : seznamPozorovatelu) {
            pozorovatel.aktualizuj();
        }
    }


}


