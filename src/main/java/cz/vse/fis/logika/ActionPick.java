package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro sbírání předmětů.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-25
 */
public class ActionPick implements IAction
{
    private Game game;
    private Inventory inventory;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionPick(Game game)
    {
        this.game = game;
        this.inventory = game.getWorld().getInventory();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>seber</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "seber";
    }

    /**
     * Metoda se pokusí sebrat předmět z aktuální lokace a uložit ho do hráčova
     * inventáře. Nejprve zkontroluje počet parametrů. Pokud nebyl zadán žádný
     * parametr <i>(tj. neznáme požadovaný předmět)</i>, nebo bylo zadáno dva a
     * více parametrů <i>(tj. hráč chce sebrat více předmětů současně)</i>, vrátí
     * chybové hlášení. Pokud byl zadán právě jeden parametr, zkontroluje, zda
     * v aktuální lokaci je předmět s daným názvem, zda je přenositelný a zda
     * na něj hráč má v inventáři místo <i>(tj. zda ho lze sebrat)</i>. Pokud ne,
     * vrátí chybové hlášení. Pokud všechny kontroly proběhnou v pořádku, provede
     * přesun předmětu z lokace do inventáře a vrátí informaci o sebrání předmětu.
     *
     * @param parameters parametry příkazu
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length < 1) {
            return "Tomu nerozumím, musíš mi říct, co mám sebrat.";
        }

        Area currentArea = game.getWorld().getCurrentArea();
        String seznam = "";
        String vystup = "";

        for (String itemName : parameters) {
            if (!currentArea.containsItem(itemName)) {
                vystup = "Předmět '" + itemName + "' tady není.\n";
                continue;
            }
            else{
                Item item = currentArea.getItem(itemName);
                if(item.getPrice().equals(""))
                {
                    if (!item.isMoveable()) {
                        vystup = "Předmět '" + itemName + "' fakt neuneseš.\n";
                        continue;
                    }
                    else{
                        if(!inventory.misto())
                        {
                            vystup = "Tvůj inventář je plný.\n";
                            continue;
                        }
                        else{
                            currentArea.removeItem(itemName);
                            inventory.addToInv(item);
                            seznam = seznam + itemName + " ";

                            vystup = "Sebral jsi předmět '" + itemName + "' a uložil ho do inventáře.\n";
                            continue;
                        }
                    }
                }
                else{
                    vystup = "Předmět '" + itemName + "' si musíš koupit.\n";
                    continue;
                }
            }

        }
        if(seznam.equals(""))
        {
            return "Do inventáře se nic nepřidalo.";
        }
        return vystup + "\n\nDo inventáře se přidali: " + seznam;
    }
}
