package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro průzkum konkrétního předmětu.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-26
 */
public class ActionInvestigate implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionInvestigate(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>prozkoumej</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "prozkoumej";
    }

    /**
     * Metoda vrátí detailní popis vybraného předmětu v aktuální lokaci.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters) {
        if (parameters.length < 1) {
            return "Tomu nerozumím, musíš mi říct, co mám prozkoumat.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím zkoumat více věcí současně.";
        }

        String itemName = parameters[0];
        Area currentArea = game.getWorld().getCurrentArea();

        if (!currentArea.containsItem(itemName)) {
            if (!game.getWorld().getInventory().containItem(itemName)) {
                return "Předmět '" + itemName + "' není v místnosti a ani v inventáři.";
            } else {
                Item item = game.getWorld().getInventory().getItem(itemName);
                return "V inventáři máš předmět '" + itemName + "'.\n" + item.getDescription();
            }
        }

        Item item = currentArea.getItem(itemName);
        if (item.getPrice().equals("")) {
            return "Díváš se na předmět '" + itemName + "'.\n" +
                    item.getDescription();
        } else {
            if (item.isNoAutomat() && item.isNoLos())
            {
                return "Díváš se na předmět '" + itemName + "'.\n" +
                        item.getDescription() + "\n" +
                        "Tento předmět můžes koupit za '" + item.getPrice() + "'.";

            }
            else
            {
                if(!item.isNoAutomat())
                {
                    return "Díváš se na předmět '" + itemName + "'.\n" +
                            item.getDescription() + "\n" +
                            "Tento automat se roztočí pomocí předmětu '" + item.getPrice() + "'.";
                }
                else
                {
                    return "Díváš se na předmět '" + itemName + "'.\n" +
                            item.getDescription() + "\n" +
                            "Tento los setřeš pomocí předmětu '" + item.getMince() + "' nebo '" + item.getMince2() + "'.\n" +
                            "Tento předmět můžes koupit za '" + item.getPrice() + "'.";
                }
            }
        }
    }
}
