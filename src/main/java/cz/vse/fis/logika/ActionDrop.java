package cz.vse.fis.logika;

/**
 * Třída implementující příkaz pro pokládání předmětů.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-26
 */

public class ActionDrop implements IAction {
    private Game game;
    private Inventory inventory;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionDrop(Game game)
    {
        this.game = game;
        this.inventory = game.getWorld().getInventory();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>poloz</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "poloz";
    }

    /**
     * Provádí příkaz "poloz". Pokud batoh neobsahuje věc, vypíše se chybová hláška.
     * V opačném případě se předmět položí do aktuální místnosti. Lze polozit jen jeden předmět.
     *
     * @param parameters jméno věci
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length < 1) {
            return "Tomu nerozumím, musíš mi říct, co mám položit.";
        }
        if(parameters.length > 1)
        {
            return "Můžeš položit pouze jeden předmět.";
        }
        String itemName = parameters[0];
        Area currentArea = game.getWorld().getCurrentArea();
        Item dropItem = inventory.getItem(itemName);

        if(dropItem == null)
        {
            return "Tahle věc v inventáři není.";
        }
        else
        {
            inventory.delItem(itemName);
            currentArea.addItem(dropItem);

            return "Položil jsi '" + itemName + "'.";
        }


    }
}
