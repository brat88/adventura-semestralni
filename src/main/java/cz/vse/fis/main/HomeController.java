package cz.vse.fis.main;

import cz.vse.fis.logika.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.application.Platform;
import javafx.scene.input.MouseEvent;
import javafx.geometry.Point2D;
import javafx.scene.image.ImageView;
import javafx.scene.control.*;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import javafx.scene.Scene;
import javafx.stage.Stage;




import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class HomeController implements Pozorovatel{

    private Game game;

    @FXML
    private TextArea vystup;

    @FXML
    private TextField vstup;

    @FXML
    private Button odesli;

    @FXML
    private ListView<Area> seznamVychodu;

    @FXML
    private ListView<Item> seznamInventare;

    @FXML
    private ImageView hrac;

    @FXML
    public void initialize()
    {
        game = new Game();
        game.getWorld().registruj(this);
        game.getWorld().getInventory().registruj(this);
        vystup.setText(game.getPrologue()+"\n\n");

        nacteniMistnosti();
        nacteniInventare();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });

        seznamVychodu.setCellFactory(new Callback<ListView<Area>, ListCell<Area>>(){
            @Override
            public ListCell<Area> call(ListView<Area> param) {
                return new ListCell<Area>() {
                    @Override
                    protected void updateItem(Area prostor, boolean b) {
                        super.updateItem(prostor,b);
                        if(prostor!=null) {
                            ImageView obrazek = new ImageView("gambler.png");
                            obrazek.setFitHeight(60);
                            obrazek.setPreserveRatio(true);
                            setText(prostor.getName());
                            setGraphic(obrazek);
                        } else {
                            setText("");
                            setGraphic(new ImageView());
                        }
                    }
                };
            }
        });
        seznamInventare.setCellFactory(new Callback<ListView<Item>, ListCell<Item>>() {
            @Override
            public ListCell<Item> call(ListView<Item> param) {
                return new ListCell<Item>() {
                    @Override
                    protected void updateItem(Item item, boolean b) {
                        super.updateItem(item, b);
                        if (item!=null) {
                            String url = item.getObrazek();
                            ImageView obrazek = new ImageView(url);
                            obrazek.setFitHeight(60);
                            obrazek.setPreserveRatio(true);
                            setText(item.getName());
                            setGraphic(obrazek);
                        } else {
                            setText("");
                            setGraphic(new ImageView());
                        }
                    }
                };
            }
        });

    }

    @FXML
    private void zobrazNapovedu() {
        Stage stage = new Stage();

        WebView view = new WebView();
        view.getEngine().load(Objects.requireNonNull(getClass().getResource("/napoveda.html")).toExternalForm());

        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.show();
    }


    private static Map<String, Point2D> getSouradniceProstoru()
    {
        Map<String, Point2D> map = new HashMap<>();
        map.put("mesto", new Point2D(190,31));
        map.put("sklad", new Point2D(32,155));
        map.put("domov", new Point2D(263,169));
        map.put("kiosek", new Point2D(145,303));
        map.put("vesnice", new Point2D(503,31));
        map.put("herna", new Point2D(505,160));
        map.put("forbes", new Point2D(528,305));
        map.put("ruleta", new Point2D(354,319));
        return map;
    }


    private void nacteniMistnosti()
    {

        Area curentArea = game.getWorld().getCurrentArea();

        seznamVychodu.getItems().addAll(curentArea.getExits());

        Point2D souradnice = getSouradniceProstoru().get(curentArea.getName());

        hrac.setLayoutX(souradnice.getX());
        hrac.setLayoutY(souradnice.getY());
    }


    public void zpracujVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
    }
    private void zpracujPrikaz(String prikaz)
    {
        String vysledek = game.processAction(prikaz);
        vystup.appendText("> " + prikaz + "\n" + vysledek + "\n\n");
        vstup.setText("");
        seznamInventare.getItems().clear();
        nacteniInventare();

        if (game.isGameOver()) {
            vystup.appendText(game.getEpilogue());
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);

        }
    }

    public void klikSeznamVychodu(MouseEvent mouseEvent) {
        Area novyProstor = seznamVychodu.getSelectionModel().getSelectedItem();
        if (novyProstor == null) return;
        String prikaz = ActionMove.NAZEV+ " " +novyProstor;
        zpracujPrikaz(prikaz);
    }

    public void klikInventar(MouseEvent mouseEvent)
    {
        String vec = seznamInventare.getSelectionModel().getSelectedItem().getName();
        if(vec == null) return;
        vstup.appendText(vec);
    }

    public void novaHra()
    {
        initialize();
        aktualizuj();
    }

    private void nacteniInventare()
    {
        Inventory aktualniInventar = game.getWorld().getInventory();
        seznamInventare.getItems().addAll(aktualniInventar.getInventar());

    }



    @Override
    public void aktualizuj() {
        System.out.println("aktualizace");
        seznamVychodu.getItems().clear();
        seznamInventare.getItems().clear();

        nacteniMistnosti();
        nacteniInventare();
    }
}

