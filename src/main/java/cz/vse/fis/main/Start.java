package cz.vse.fis.main;

import cz.vse.fis.logika.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import cz.vse.fis.uiText.TextUI;


/**
 * Spouštěcí třída aplikace.
 *
 * @author Jan Říha
 * @version LS-2021, 2021-05-10
 */
public class Start extends Application
{


    /**
     * Spouštěcí metoda aplikace. Vyhodnotí parametry, se kterými byla aplikace
     * spuštěna, a na základě nich rozhodne, jakou operaci provede <i>(hra, výpis
     * testovacích scénářů, spuštění testovacích scénářů)</i>.
     * <p>
     * Pokud byla aplikace spuštěna s nepodporovanými parametry, vypíše nápovědu
     * a skončí.
     *
     * @param args parametry aplikace z příkazové řádky
     */
    public static void main(String[] args)
    {

        if(args.length>0 && args[0].equals("-text"))
        {
            Game game = new Game();
            TextUI ui = new TextUI(game);

            ui.play();
        }
        else {
            launch();
        }
        System.exit(0);

        /*

        } else if (args.length == 1 && args[0].equalsIgnoreCase("SHOW_SCENARIOS")) {
            //Runner runner = new Runner();

            System.out.println(runner.showAllScenarios());
        } else if (args.length == 1 && args[0].equalsIgnoreCase("RUN_SCENARIOS")) {
            Runner runner = new Runner();

            System.out.println(runner.runAllScenarios());
        } else {
            System.out.println("Hra byla spuštěna s nepodporovanými parametry!");

            System.out.println("\nAplikaci je možné spustit s následujícími parametry:");
            System.out.println("  <bez parametrů>     : Spustí hru");
            System.out.println("  SHOW_SCENARIOS      : Vypíše kroky testovacích scénářů");
            System.out.println("  RUN_SCENARIOS       : Provede testy testovacích scénářů");
        }*/


    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/home.fxml"));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }
}
