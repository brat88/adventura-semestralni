package cz.vse.fis.logika;


/**
 * Třída implementující kontrolu předmětů v inventáři v lokaci po vykonání příkazu.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-27
 */

public class CurrentAreaInventoryChecker implements IChecker{
    @Override
    public CheckResult check(Step step, Game game, String actionResult)
    {


        String message = "Inventář obsahuje    : " + game.getWorld().getInventory() + "\nVýsledek   : ";

        if (step.getInventory() == null) {
            return new CheckResult(true, message + "(nekontroluje se)");
        }

        String vysledek = step.getInventory();

        if (vysledek.equals(vysledek)) {
            return new CheckResult(true, message + "OK");
        }

        return new CheckResult(false, message + "CHYBA, test očekává: " + String.join(", ", vysledek));
    }
}
