package cz.vse.fis.logika;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování třídy {@link ActionMove}.
 *
 * @author Jan Říha
 * @version LS-2021, 2021-05-10
 */
public class ActionMoveTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();

        Area house = new Area("dům", "Tady bydlíš, proto tu i začíná hra.");
        Area forest = new Area("les", "Toto je les kolem tvého domu, rostou zde houby, žije neškodná havěť apod.");

        house.addExit(forest);
        forest.addExit(house);

        game.getWorld().setCurrentArea(house);
    }

    @Test
    public void testIncorrectParameters()
    {
        assertEquals("Tomu nerozumím, musíš mi říct, kam mám jít.", game.processAction("jdi"));
        assertEquals("dům", game.getWorld().getCurrentArea().getName());

        assertEquals("Tomu nerozumím, neumím jít do více lokací současně.", game.processAction("jdi a b c"));
        assertEquals("dům", game.getWorld().getCurrentArea().getName());

        assertEquals("Do lokace 'abc' se odsud jít nedá.", game.processAction("jdi abc"));
        assertEquals("dům", game.getWorld().getCurrentArea().getName());
    }

    @Test
    public void testCorrectParameters()
    {
        assertTrue(game.processAction("jdi les").toLowerCase().contains("toto je les"));
        assertEquals("les", game.getWorld().getCurrentArea().getName());

        assertTrue(game.processAction("jdi dům").toLowerCase().contains("tady bydlíš"));
        assertEquals("dům", game.getWorld().getCurrentArea().getName());
    }
}
