package cz.vse.fis.logika;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Testovací třída pro komplexní otestování třídy {@link Inventory}.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-28
 */


public class InventoryTest {

    /**
     * Testuje, zda do batohu nelze přidat víc jak maximální počet předmětů.
     */
    @Test
    public void testMaxSize()
    {
        Inventory inventory = new Inventory();
        Item item1 = new Item("item1", "", null, null,"");
        Item item2 = new Item("item2", "", null, null, "");
        Item item3 = new Item("item3", "", null, null, "");
        Item item4 = new Item("item4", "", null, null, "");
        Item item5 = new Item("item5", "", null, null,"");
        Item item6 = new Item("item6", "", null, null, "");
        Item item7 = inventory.addToInv(item1);
        assertEquals(item7, item7);
        Item item8 = inventory.addToInv(item2);
        assertEquals(item8, item8);
        Item item9 = inventory.addToInv(item3);
        assertEquals(item9, item9);
        Item item10 = inventory.addToInv(item4);
        assertEquals(item10, item10);
        Item item11 = inventory.addToInv(item5);
        assertEquals(item11, item11);
        assertNull(inventory.addToInv(item6));
    }

    /**
     * Testuje vložení věci do batohu
     */
    @Test
    public void testAddItem()
    {
        Inventory inventory = new Inventory();
        Item item1 = new Item("item1", "", null, null, "");
        assertEquals(item1, inventory.addToInv(item1));
        assertEquals(true, inventory.containItem("item1"));
    }

    /**
     * Testuje smazání věci z batohu
     */
    @Test
    public void testDelItem()
    {
        Inventory inventory = new Inventory();
        Item item1 = new Item("item1", "", null, null, "");
        inventory.addToInv(item1);
        assertEquals(item1, inventory.delItem("item1"));
        assertEquals("", inventory.getItems());
    }

}
