package cz.vse.fis.logika;

/**
 * Třída implementující kontrolu zda je konec hry.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-28
 */

public class GameOverChecker implements IChecker {
    @Override
    public CheckResult check(Step step, Game game, String actionResult)
    {

        String message = "GAME OVER     : " + game.isGameOver() + "\nVýsledek   : ";

        if(step.getGameOver()) {
            return new CheckResult(true, message + "(nekontroluje se)");
        }

        if (game.isGameOver() && step.getGameOver()) {
            return new CheckResult(true, message + "OK");
        }
        if (!game.isGameOver() && step.getGameOver()) {
            return new CheckResult(false, message + "CHYBA, test očekává: " + step.getGameOver());

        }
        return new CheckResult(true, message + "(nekontroluje se)");
    }
}
