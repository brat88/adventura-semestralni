package cz.vse.fis.logika;



import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * Třída implementující kontrolu předmětů v lokaci po vykonání příkazu.
 *
 * @author Tomáš Branžovský
 * @version LS-2021, 2021-05-27
 */

public class CurrentAreaItemsChecker implements IChecker {
    @Override
    public CheckResult check(Step step, Game game, String actionResult)
    {
        Set<String> itemNames = new TreeSet<>();
        for (Item item : game.getWorld().getCurrentArea().getItems()) {
            itemNames.add(item.getName());
        }

        String message = "Předměty    : " + String.join(", ", itemNames) + "\nVýsledek   : ";

        if (step.getItems() == null) {
            return new CheckResult(true, message + "(nekontroluje se)");
        }

        Set<String> expectedNames = new TreeSet<>(Arrays.asList(step.getItems()));

        if (itemNames.equals(expectedNames)) {
            return new CheckResult(true, message + "OK");
        }

        return new CheckResult(false, message + "CHYBA, test očekává: " + String.join(", ", expectedNames));
    }
}
